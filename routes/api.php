<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});
//Route::get('/test', [\App\Http\Controllers\Api\AdminController::class, 'test']);

//Route::middleware(['cors'])->prefix('v1')->middleware(['cors'])->name('api.')->group(function (){\
Route::group(['prefix' => 'v1',], function () {
    Route::post('/test', [\App\Http\Controllers\Api\AdminController::class, 'test']);
    Route::post('email', [\App\Http\Controllers\Auth\AdminController::class, 'email'])->name('email');

    Route::prefix('admin')->name('admin.')->group(function (){
        Route::post('login', [\App\Http\Controllers\Auth\AdminController::class, 'login'])->name('login');
        Route::post('regis', [\App\Http\Controllers\Auth\AdminController::class, 'register'])->name('regis');
        Route::prefix('operate')->middleware(['admins'])->group(function (){
            Route::post('logout', [\App\Http\Controllers\Auth\AdminController::class, 'logout'])->name('logout');
            Route::post('profile', [\App\Http\Controllers\Auth\AdminController::class, 'profile'])->name('profile');


            Route::post('student-all', [\App\Http\Controllers\Api\AdminController::class, 'studentAll'])->name('studentAll');

            Route::prefix('referral')->name('referral.')->group(function (){
                Route::post('index',[\App\Http\Controllers\Api\ReferralController::class,'index']);
                Route::post('store',[\App\Http\Controllers\Api\ReferralController::class,'store']);
            });

            Route::prefix('space')->name('space.')->group(function (){
                Route::post('create',[\App\Http\Controllers\Api\SpaceController::class,'create']);
                Route::post('destroy',[\App\Http\Controllers\Api\SpaceController::class,'destroy']);
                Route::post('show',[\App\Http\Controllers\Api\SpaceController::class,'show']);

            });
	        Route::post('show/detail/exam',[\App\Http\Controllers\Api\QuestionController::class,'detailExam']);
	        Route::post('show/detail/space',[\App\Http\Controllers\Api\QuestionController::class,'detailSpace']);
	        Route::post('show/detail/question',[\App\Http\Controllers\Api\QuestionController::class,'detailQuestion']);
            Route::post('history/exam',[\App\Http\Controllers\Api\QuestionController::class,'historyExamAdmin']);
            Route::post('history/exam/detail',[\App\Http\Controllers\Api\QuestionController::class,'flutter.historyExamAdmin']);

            Route::prefix('question')->name('question.')->group(function (){
	            Route::post('destroy',[\App\Http\Controllers\Api\QuestionController::class,'destroy']);
	            Route::post('exam',[\App\Http\Controllers\Api\QuestionController::class,'category']);
	            Route::post('destroy/exam',[\App\Http\Controllers\Api\QuestionController::class,'destroyExam']);
                Route::post('show/exam',[\App\Http\Controllers\Api\QuestionController::class,'showCategory']);
                Route::post('create',[\App\Http\Controllers\Api\QuestionController::class,'create']);
                Route::post('answer/create',[\App\Http\Controllers\Api\QuestionController::class,'answer']);
                Route::post('answer/create/key',[\App\Http\Controllers\Api\QuestionController::class,'answerKey']);
                Route::post('answer/show',[\App\Http\Controllers\Api\QuestionController::class,'showAnswer']);
                Route::post('answer/show/byUSer',[\App\Http\Controllers\Api\QuestionController::class,'showAnswerByUser']);
//                Route::post('answer/he',[\App\Http\Controllers\Api\QuestionController::class,'showAnswerHe']);

//                Route::post('store',[\App\Http\Controllers\Api\ReferralController::class,'store']);

            });

        });
    });

    Route::post('admin/operate/question/answer/he',[\App\Http\Controllers\Api\QuestionController::class,'showAnswerHe']);


    Route::prefix('student')->name('student.')->group(function (){

        Route::post('login', [\App\Http\Controllers\Auth\StudentController::class, 'login'])->name('login');
        Route::post('regis', [\App\Http\Controllers\Auth\StudentController::class, 'register'])->name('regis');

        Route::prefix('operate')->middleware(['students'])->group(function (){
            Route::post('logout', [\App\Http\Controllers\Auth\StudentController::class, 'logout'])->name('logout');
            Route::post('profile', [\App\Http\Controllers\Auth\StudentController::class, 'profile'])->name('profile');
            Route::post('profile/edit', [\App\Http\Controllers\Auth\StudentController::class, 'EditProfile'])->name('EditProfile');
            Route::post('profile/edit/img', [\App\Http\Controllers\Auth\StudentController::class, 'EditProfileImg'])->name('EditProfileImg');

            Route::post('profile/reset/password', [\App\Http\Controllers\Auth\StudentController::class, 'resetPassword'])->name('resetPassword');

            Route::post('submitExam', [\App\Http\Controllers\Api\ExamController::class, 'submitExam'])->name('submitExam');
            Route::post('on-going/exam', [\App\Http\Controllers\Api\ExamController::class, 'onGoingExam'])->name('onGoingExam');
          Route::post('answer/he-create',[\App\Http\Controllers\Api\QuestionController::class,'answerHeKey']);
          Route::post('show/exam',[\App\Http\Controllers\Api\QuestionController::class,'showCategory']);
          Route::post('counting/exam/pre-test',[\App\Http\Controllers\Api\QuestionController::class,'counting']);
          Route::post('counting/exam/toefl',[\App\Http\Controllers\Api\QuestionController::class,'countingToefl']);
          Route::post('history/exam',[\App\Http\Controllers\Api\QuestionController::class,'historyExam']);

        });
    });

});
