<?php

namespace Database\Seeders;

use App\Models\Answer;
use App\Models\Category;
use App\Models\HeAnswer;
use App\Models\KeyAnswer;
use App\Models\Question;
use App\Models\Space;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ExamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ran = ['toefl','pre-test'];

        $alapabet = ['A','B','C','D'];

        $section = ['reading','structure','listening'];
        $longRan = ['ini pertanyaan soal ceritaaaaaa',''];

        for ($i=1;$i<=10;$i++){
            $randomElement = $ran[array_rand($ran, 1)];
            $name = 'ini ujian ' .$randomElement.' ke : '.$i;
            $data = new Category();
            $data->categories = $randomElement;
            $data->name =  $name;
            $data->slug = \Str::slug($name);
            $data->time = 120 ;

            $data->save();

            if ($randomElement == 'pre-test'){
                for ($as=0;$as <=3; $as++ ){
                    $cerita = $longRan[array_rand($longRan, 1)];
                    $pretestSpace = new Space();
                    $pretestSpace->category_id = $data->id;
                    $pretestSpace->long_question = $cerita;
                    $pretestSpace->section = 'reading';
                    $pretestSpace->save();

                    $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    if ($pretestSpace->long_question == ''){
                        $question = new Question();
                        $question->space_id = $pretestSpace->id;
                        $question->question = 'ini pertanyaan '.substr(str_shuffle($permitted_chars), 0, 16).' ? ';
                        $question->save();

                        for ($q='A';$q<='D';$q++){
                            $answer = new Answer();
                            $answer->question_id = $question->id;
                            $answer->alphabet = $q;
                            $answer->answer = 'ini jawaban '. $q;
                            $answer->save();
                        }

                        $key_answer = new KeyAnswer();
                        $key_answer->alphabet_key = $alapabet[array_rand($alapabet, 1)];
                        $key_answer->question_id = $question->id;
                        $key_answer->save();
                    }else{
                        for ($k=1;$k<=10;$k++){
                            $question = new Question();
                            $question->space_id = $pretestSpace->id;
                            $question->question = 'ini pertanyaan '.substr(str_shuffle($permitted_chars), 0, 16).' ? ';
                            $question->save();
                            for ($q='A';$q<='D';$q++){
                                $answer = new Answer();
                                $answer->question_id = $question->id;
                                $answer->alphabet = $q;
                                $answer->answer = 'ini jawaban '. $q;
                                $answer->save();
                            }
                            $key_answer = new KeyAnswer();
                            $key_answer->alphabet_key = $alapabet[array_rand($alapabet, 1)];
                            $key_answer->question_id = $question->id;
                            $key_answer->save();

                        }
                    }


                }
            }else{

                for ($as=0;$as <=3; $as++ ){
                    $cerita = $longRan[array_rand($longRan, 1)];
                    $selecSection = $section[array_rand($section, 1)];

                    $pretestSpace = new Space();
                    $pretestSpace->category_id = $data->id;
                    $pretestSpace->long_question = $cerita;
                    $pretestSpace->section = $selecSection;
                    $pretestSpace->save();

                    $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

                    if ($pretestSpace->long_question == ''){
                        $question = new Question();
                        $question->space_id = $pretestSpace->id;
                        $question->question = 'ini pertanyaan '.substr(str_shuffle($permitted_chars), 0, 16).' ? ';
                        $question->save();

                        for ($q='A';$q<='D';$q++){
                            $answer = new Answer();
                            $answer->question_id = $question->id;
                            $answer->alphabet = $q;
                            $answer->answer = 'ini jawaban '. $q;
                            $answer->save();
                        }

                        $key_answer = new KeyAnswer();
                        $key_answer->alphabet_key = $alapabet[array_rand($alapabet, 1)];
                        $key_answer->question_id = $question->id;
                        $key_answer->save();
                    }else{
                        for ($k=1;$k<=10;$k++){
                            $question = new Question();
                            $question->space_id = $pretestSpace->id;
                            $question->question = 'ini pertanyaan '.substr(str_shuffle($permitted_chars), 0, 16).' ? ';
                            $question->save();
                            for ($q='A';$q<='D';$q++){
                                $answer = new Answer();
                                $answer->question_id = $question->id;
                                $answer->alphabet = $q;
                                $answer->answer = 'ini jawaban '. $q;
                                $answer->save();
                            }
                            $key_answer = new KeyAnswer();
                            $key_answer->alphabet_key = $alapabet[array_rand($alapabet, 1)];
                            $key_answer->question_id = $question->id;
                            $key_answer->save();

                        }
                    }


                }


            }


        }
    }
}
