<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\HeAnswer;
use App\Models\Question;
use App\Models\Space;
use App\Models\Student;
use App\Models\Test;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class StartToeflSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $alapabet = ['A','B','C','D'];


        $user = Student::whereEmail('twentrish@student.com')->first();

//        $category = Category::where('categories','toefl')->first();

        $test = new Test();
        $test->category_id = '3';
        $test->student_id = $user->id;
        $test->session = 'toeflSession';

        $test->save();

        $spaces = Space::where('category_id',$test->category_id)->get();

        foreach ($spaces as $space){
            $questionAnswers = Question::where('space_id',$space->id)->get();

            foreach ($questionAnswers as $ans){
                $he_answer = new HeAnswer();
                $he_answer->question_id = $ans->id;
                $he_answer->test_id = $test->id;
                $he_answer->he_answer = $alapabet[array_rand($alapabet, 1)];
                $he_answer->save();
            }
        }
    }
}
