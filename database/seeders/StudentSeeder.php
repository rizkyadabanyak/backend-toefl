<?php

namespace Database\Seeders;

use App\Models\Student;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new Student();

        $data->name = 'rizky putra r';
        $data->email = 'twentrish@student.com';
        $data->password = Hash::make('123123');
        $data->token = '123123';
        $data->img = 'default';
        $data->save();

        $dataBuatUjian = new Student();

        $dataBuatUjian->name = 'Studuent yang udah ujian';
        $dataBuatUjian->email = 'udhujian@student.com';
        $dataBuatUjian->password = Hash::make('123123');
        $dataBuatUjian->token = '1231234';
        $dataBuatUjian->img = 'default';
        $dataBuatUjian->save();


        $datas = new User();

        $datas->name = 'ini admin';
        $datas->email = 'rizky@admin.com';
        $datas->password = Hash::make('rizky@admin.com');
        $datas->token = '123123';
//        $data->img = 'default';
        $datas->save();
    }
}
