<?php

namespace Database\Seeders;

use App\Models\Question;
use App\Models\Space;
use App\Models\Test;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CountPreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category_id= '5';

        $dataCount = Space::with('questions')->whereCategoryId($category_id)->get();

        $tmpQuestions = [];
        $i =0;
        foreach ($dataCount as $a){
            //	  $indexSpace[$i++] = $a->id;
            $as = Question::with(['answer','key','he_answer'])->whereSpaceId($a->id)->get();
            foreach ($as as $ds){
                $tmpQuestions[$i++] = $ds;
            }
        }

        $countQuestion = count($tmpQuestions);

        $questionTrue = 0;
        foreach ($tmpQuestions as $qstin){
            //	      dd($qstin->key[0]->alphabet_key);
            //	      dd($qstin->he_answer[0]->he_answer);
            if ($qstin->key[0]->alphabet_key == $qstin->he_answer[0]->he_answer){
                $questionTrue++;
            }
        }

        $nilai = (100/$countQuestion)*$questionTrue;
        $exam = Test::find('1');

        $exam->score = $nilai;
        $exam->status = 'finished';
        $exam->save();

    }
}
