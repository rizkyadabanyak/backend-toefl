<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Http\Request;

class Question extends Model
{
    use HasFactory;
//    protected $with = ['answer','key','he_answer'];
    protected $hidden = ['created_at','updated_at'];

    protected $guarded = [];
//    protected $visible = ['id'];
//    public function getRouteKeyName()
//    {
//        return 'id';
//    }
    public function answer(){
        return $this->hasMany(Answer::class)->orderBy('alphabet','ASC');
    }
    public function key(){
        return $this->hasMany(KeyAnswer::class,);
    }
    public function he_answer(){

        //ini berubah

//        $exam = Test::whereSession('cobaSessionPretest')->first(); //buat dummy
            $exam = Test::whereSession(request()->session)->first(); // yg asli

        return $this->hasMany(HeAnswer::class)->where('test_id',$exam->id); //yg asli
//        return $this->hasMany(HeAnswer::class)->where('test_id',$exam->id); // dummy
    }
    public function he_answerAdmin()
    {
        $exam = Test::whereCategoryId(request()->category_id)
	  ->first();

//        dd($exam);
        return $this->hasMany(HeAnswer::class)->where('test_id',$exam->id);
    }

    }
