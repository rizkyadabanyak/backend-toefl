<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Space extends Model
{
    use HasFactory;

    protected $guarded = [];
//    protected $with = ['questions','questions3'];
//    protected $with = ['questions'];

//    protected $visible = ['id','category_id','long_question','questions'];
    protected $hidden = ['created_at','updated_at'];

    public function questions(){
//        return $this->hasMany(Question::class);
        return $this->hasMany(Question::class);
    }
}
