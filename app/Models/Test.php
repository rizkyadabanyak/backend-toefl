<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    use HasFactory;

    protected $with=['categories','student'];

    public function categories(){
        return $this->belongsTo(Category::class,'category_id');
    }
    public function student(){
        return $this->belongsTo(Student::class,'student_id');
    }
}
