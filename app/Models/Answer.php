<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    use HasFactory;
//    protected $with = ['questions'];
    protected $hidden = ['created_at','updated_at'];
    protected $guarded = [];
//    public function questions(){
//        return $this->belongsTo(Question::class,'question_id');
//    }
}
