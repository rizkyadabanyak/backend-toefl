<?php

namespace App\Http\Controllers\Api;

use App\Models\Referral;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReferralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Referral::orderBy('created_at','DESC')->get();

        return response()->json([
            'data' => $data,
            'status' => 'success',
            'message' => 'success get data'
        ], 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Referral();

        $num_str = sprintf("%06d", mt_rand(1, 999999));
//        dd($num_str);
        $data->status = 'active';
        $data->referral = $num_str;
        $data->save();

        return response()->json([
            'data' => $data,
            'status' => 'success',
            'message' => 'success create referral'
        ], 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
