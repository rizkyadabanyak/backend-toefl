<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Question;
use App\Models\Student;
use App\Models\Test;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function studentAll(){
        $data = Student::select(['id','img','name','email'])->orderBy('created_at','DESC')->get();

        return response()->json([
            'data' => $data,
            'status' => 'success',
            'message' => 'success get data'
        ], 200);
    }

    public function test(){
//        dd('dqw');
        $data = Test::whereStatus('on-going')->get();
        $dt = Carbon::now();

        foreach ($data as $a){
            $time = (int)$a->categories->time;
//            dd($time);
            $akhir = Carbon::parse($a->created_at);

            $akhir->addMinutes($time);
//            dd([$dt->toDateTimeString(),$akhir->toDateTimeString()]);
            if ($dt->getTimestamp() >= $akhir->getTimestamp()){
                $a->status = 'failed';

                $a->save();
            }

        }
    }


}
