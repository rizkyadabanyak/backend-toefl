<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Space;
use App\Models\Student;
use App\Models\Test;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ExamController extends Controller
{
    public function submitExam (Request $request){
        $a = Student::select(['id','name','email'])->whereToken($request->token)->first();
        $id = $a->id;

//        dd(uniqid());
        $cek = Test::whereCategoryId($request->category_id)->whereStudentId($id)->whereStatus('on-going')->count();
//        dd($cek);
        if ($cek == 1 ){
	  return response()->json([
	      'status' => 'danger',
	      'message' => "can't join exam "
	  ], 200);
        }

        $data = new Test();
        $data->category_id = $request->category_id;
        $data->student_id = $id;
        $data->status = 'on-going';
        $data->session = uniqid();
        $data->save();

        return response()->json([
	  'data' => $data,
	  'status' => 'success',
	  'message' => 'success join exam'
        ], 200);

    }

    public function onGoingExam (Request $request){
//        dd($request->uniq_id);


        $a = Student::select(['id','name','email'])->whereToken($request->token)->first();
        $id = $a->id;


        $exam = Test::whereSession($request->session)
//            ->where('status','on-going')
//	  ->with([
//	      'categories.spaces.questions',
//	      'categories.spaces.questions.answer',
//	      'categories.spaces.questions.he_answer'
//	  ])
	  ->first();
//        dd($exam);

        if ($exam == null){
          return response()->json([
              'status' => 'danger',
              'message' => 'exam not found'
          ], 200);
        }

//        $tmpExamp =  Carbon::parse($exam->created_at);
        $tmpCategory = Category::find($exam->category_id);
//        $dt= Carbon::parse($tmpCategory->time);
//        dd($dt->valueOf());
//        dd($dt->getPreciseTimestamp(3));
//        $timestamp = (int) round(Carbon::parse($tmpCategory->time)->format('Uu') / pow(10, 6 - 3));

//        $nowInMilliseconds = (int) ($dt->timestamp . str_pad($dt->milli, 3, '0', STR_PAD_LEFT));

        $milisec = $tmpCategory->time * 60000;
        $id = $request->number_space;
        $indexSpace = [];


        $data = Space::with(['questions.answer','questions.he_answer'])->whereCategoryId($exam->category_id)->where('id','=',$id)->get();
//        $data = Category::find($request->category_id);
        $dataCount = Space::with('questions')->whereCategoryId($exam->category_id)->get();

        $i =0;
        $countQuestion = [];
        foreach ($dataCount as $a){
            $as=  Space::with('questions')->where('id',$a->id)->first();
//            dd(count($as->questions));
	        $indexSpace[$i] = $a->id;
            $countQuestion[$i] = count($as->questions);

            $i++;
        }

        return response()->json([
          'data' => [
              'time_exam_in_milliseconds' => $milisec,
              'info_exam' => $exam,
              'question' => $data
          ],
          'count_space' => count($dataCount),
          'index_space' => $indexSpace,
          'countQuestionBySpace' => $countQuestion,
          'now_space' => $request->number_space,
          'status' => 'success',
          'message' => 'success get data'
        ], 200);
    }
}
