<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Answer;
use App\Models\Category;
use App\Models\Exam;
use App\Models\HeAnswer;
use App\Models\KeyAnswer;
use App\Models\Question;
use App\Models\QuestionExam;
use App\Models\Space;
use App\Models\Student;
use App\Models\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class QuestionController extends Controller
{
    public function destroy(Request $request){
        $request->validate([
            'id' => 'required'
        ]);


        $data = Question::find($request->id);
        $data->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'success delete data'
        ], 200);
    }

    public function destroyExam(Request $request){

        $request->validate([
            'id' => 'required'
        ]);


        $data = Category::find($request->id);

        $data->soft_delete = '1';
        $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'success delete data'
        ], 200);
    }

    public function detailQuestion(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);
        $data = Question::find($request->id);

        return response()->json([
            'data' => $data,
            'status' => 'success',
            'message' => 'success get Question'
        ], 200);
    }
    public function detailSpace(Request $request){
        $request->validate([
            'id' => 'required'
        ]);
        $data = Space::find($request->id);

        return response()->json([
            'data' => $data,
            'status' => 'success',
            'message' => 'success get space'
        ], 200);
    }

    public function detailExam(Request $request){
        $request->validate([
	        'id' => 'required'
        ]);
        $data = Category::find($request->id);

        return response()->json([
          'data' => $data,
          'status' => 'success',
          'message' => 'success get categories'
        ], 200);
    }

    public function historyExamAdmin(Request $request){

        $data = Test::orderBy('created_at','DESC')->get();
        return response()->json([
          'data' => $data,
          'status' => 'success',
          'message' => 'success show categories'
        ], 200);
    }

    public function historyExam(Request $request){

//        dd($request->token);
        $a = Student::select(['id','name','email'])->whereToken($request->token)->first();
        $id = $a->id;

        $data = Test::whereStudentId($id)->orderBy('created_at','DESC')->get();

        return response()->json([
          'data' => $data,
          'status' => 'success',
          'message' => 'success show categories'
        ], 200);
    }

    public function countingToefl(Request $request){
        $exam = Test::whereSession($request->session)->first();
        $dataCount = Space::with('questions')->whereCategoryId($exam->category_id)->get();

        $tmpQuestions = [];
        $i =0;
        $listening = [];
        $reading = [];
        $structure = [];
        foreach ($dataCount as $a){
            //	  $indexSpace[$i++] = $a->id;]
            $as = Question::with(['answer','key','he_answer'])->whereSpaceId($a->id)->get();

            foreach ($as as $ds){
                if ($a->section == 'reading'){
                    $reading[$i++] = $ds;
                }elseif ($a->section == 'listening'){
                    $listening[$i++] = $ds;
                }elseif ($a->section == 'structure'){
                    $structure[$i++] = $ds;
                }
            }
        }


        $ArrayNilaiListening = [24,25,26,27,28,29,30,31,32,32,33,35,37,38,39,41,41,42,43,44,45,45,46,47,47,48,48,49,49,50,51,51,52,52,53,54,54,55,56,57,57,58,59,60,61,62,63,65,66,67,68];
        $ArrayNilaiStructute = [20,20,21,22,23,25,26,27,29,31,33,35,36,37,38,40,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,60,61,63,65,67,68];
        $ArrayNilaiReading = [21,22,23,23,24,25,26,27,28,28,29,30,31,32,34,35,36,37,38,39,40,41,42,43,43,44,45,46,46,47,48,48,49,50,51,52,52,53,54,54,55,56,57,58,59,60,61,63,65,66,67];

        $listeningTrue = 0;
        $readingTrue = 0;
        $structureTrue = 0;
        foreach ($listening as $listen){
            if ($listen->key[0]->alphabet_key == $listen->he_answer[0]->he_answer){
                $listeningTrue++;
            }
        }
        foreach ($structure as $strc){
            if ($strc->key[0]->alphabet_key == $strc->he_answer[0]->he_answer){
                $structureTrue++;
            }
        }

        foreach ($reading as $read){
            if ($read->key[0]->alphabet_key == $read->he_answer[0]->he_answer){
                $readingTrue++;
            }
        }
        $nilaiListening = $ArrayNilaiListening[$listeningTrue];
        $nilaiReading = $ArrayNilaiReading[$readingTrue];
        $nilaiStucture = $ArrayNilaiReading[$structureTrue];

        $total = (($nilaiListening + $nilaiReading + $nilaiStucture) * 10) / 3;


        $exam->score = $total;
        $exam->status = 'finished';
        $exam->save();

        return response()->json([
            'data' => $total,
            'status' => 'success',
        ], 200);
    }

    public function counting(Request $request){
        $exam = Test::whereSession($request->session)->first();
//        dd($exam);

//        $data = Space::with(['questions.answer','questions.he_answer','questions.key'])->whereCategoryId($exam->category_id)->get();
//        $data = Question::
        $indexSpace = [];

        $dataCount = Space::with('questions')->whereCategoryId($exam->category_id)->get();

        $tmpQuestions = [];
        $i =0;
        foreach ($dataCount as $a){
        //	  $indexSpace[$i++] = $a->id;
              $as = Question::with(['answer','key','he_answer'])->whereSpaceId($a->id)->get();
              foreach ($as as $ds){
                  $tmpQuestions[$i++] = $ds;
              }
        }

        $countQuestion = count($tmpQuestions);
//        dd($tmpQuestions);

        $questionTrue = 0;
        foreach ($tmpQuestions as $qstin){
    //	      dd($qstin->key[0]->alphabet_key);
    //	      dd($qstin->he_answer[0]->he_answer);
            if ($qstin->key[0]->alphabet_key == $qstin->he_answer[0]->he_answer){
              $questionTrue++;
            }
        }

        $nilai = (100/$countQuestion)*$questionTrue;

        $exam->score = $nilai;
        $exam->status = 'finished';
        $exam->save();

        return response()->json([
          'data' => $nilai,
          'status' => 'success',
        ], 200);
    }
    public function showCategory(Request $request){

        $data = Category::whereCategories($request->type)->where('soft_delete','0')->orderBy('created_at','DESC')->get();

        return response()->json([
	  'data' => $data,
	  'status' => 'success',
	  'message' => 'success show categories'
        ], 200);
    }
    public function category(Request $request){
        $request->validate([
            'name' => 'required',
            'time' => 'required',
        ]);

        $data = Category::updateOrCreate([
            'id' => $request->id
        ], [
            'name' => $request->name,
            'categories' => $request->option,
            'slug' => \Str::slug($request->name),
            'time' => $request->time
        ]);


        return response()->json([
            'data' => $data,
            'status' => 'success',
            'message' => 'success create category'
        ], 200);

    }

    public function create(Request $request){
        $request->validate([
            'space_id' => 'required',
            'question' => 'required',
        ]);

        $data = Question::updateOrCreate([
            'id' => $request->id
        ], [
            'space_id' => $request->space_id,
            'question' => $request->question,

        ]);

        return response()->json([
            'data' => $data,
            'status' => 'success',
            'message' => 'success create Question'
        ], 200);
    }
    public function answer(Request $request){
        $request->validate([
            'question_id' => 'required',
            'alphabet' => 'required',
            'answer' => 'required',
        ]);

        $data = Answer::updateOrCreate([
            'id' => $request->id
        ],[
            'question_id' => $request->question_id,
            'alphabet' => $request->alphabet,
            'answer' => $request->answer,
        ]);

        return response()->json([
            'data' => $data,
            'status' => 'success',
            'message' => 'success create answers'
        ], 200);

    }
    public function answerKey(Request $request){
        $data = KeyAnswer::updateOrCreate([
            'question_id' => $request->question_id
        ], [
            'alphabet_key' => $request->alphabet_key,
        ]);

//        $data->alphabet_key = $request->alphabet_key;
//        $data->save();

        return response()->json([
            'data' => $data,
            'status' => 'success',
            'message' => 'success create Question'
        ], 200);
    }

    public function showAnswer(Request $request){
//        $data = Question::whereSpaceId($request->space_id)->with(['key'])->paginate(10);
        $id = $request->number_space;
        $indexSpace = [];

        $data = Space::with(['questions.answer','questions.key'])->whereCategoryId($request->category_id)->where('id','=',$id)->get();
//        $data = Space::with(['questions.answer','questions.he_answerAdmin','questions.key'])->whereCategoryId($request->category_id)->where('id','=',$id)->get();
//        $data = Category::find($request->category_id);
        $dataCount = Space::with('questions')->whereCategoryId($request->category_id)->get();

        $i =0;
        foreach ($dataCount as $a){
            $indexSpace[$i++] = $a->id;
        }

        return response()->json([
            'data' => $data,
            'count_space' => count($dataCount),
            'index_space' => $indexSpace,
            'now_space' => $request->number_space,
            'status' => 'success',
            'message' => 'success get data'
        ], 200);

//        return new \App\Http\Resources\Question($id);

    }

    public function showAnswerHe(Request $request)
    {
        if ($request->token){
            $a = Student::select(['id','name','email'])->whereToken($request->token)->first();
            $id = $a->id;

	  $exam = Test::whereSession($request->session)
	      ->whereStudentId($id)
	      ->first();

        }else{
//            $id = $request->test_id;
	  $exam = Test::whereSession($request->session)
	      ->first();
        }

//        dd($exam);
        if ($exam == null){
	  return response()->json([
	      'status' => 'danger',
	      'message' => 'exam not found'
	  ], 200);
        }

        $id = $request->number_space;


        $data = Space::with(['questions.answer','questions.he_answer','questions.key'])->whereCategoryId($exam->category_id)->where('id','=',$id)->get();
//        $data = Category::find($request->category_id);
        $indexSpace = [];

//        dd($id);
        $dataCount = Space::with('questions')->whereCategoryId($exam->category_id)->get();

//        dd($dataCount);

        $tmpCountQuestion = [];
        $tmpAnswer = [];
        $i =0;

        $nm = 0;

        foreach ($dataCount as $a){

//	  dd($a->id);
	  $indexSpace[$i] = $a->id;

            $qs = Question::with(['he_answer'])->whereSpaceId($a->id)->get();

//	  55 58 57
//            $qs = Question::with(['he_answer'])->whereSpaceId(57)->get();

            $x =0;
            $lo = [];


//	  if ()
            foreach ($qs as $as){

	      if (count($as->he_answer) != 0){
		$lo[$x++] = $as->he_answer[0]->he_answer;
	      }else{
		$lo[$x++] = 'null';
	      }
	      $nm++;
            }
//
            $tmpAnswer[$i] = $lo;
            $tmpCountQuestion[$i] = count($qs);
	  $i++;
        }
//        dd($nm);


        return response()->json([
	  'data' => [
	      'info_exam' => $exam,
	      'question' => $data
	  ],
          'count_space' => count($dataCount),
          'index_space' => $indexSpace,
          'countQuestionBySpace' => $tmpCountQuestion,
          'now_space' => $request->number_space,
          'tmpAnswer' => $tmpAnswer,
          'status' => 'success',
          'message' => 'success get data'
        ], 200);


        return response()->json([
            'data' => $data,
            'status' => 'success',
            'message' => 'success get data'
        ], 200);


    }

    public function answerHeKey (Request $request){

        $tmp = $request->data;

        $count = count($tmp);

//        dd();
//        $exam = Test::whereSession($request->session)
////	  ->with([
////	      'categories.spaces.questions',
////	      'categories.spaces.questions.answer',
////	      'categories.spaces.questions.he_answer'
////	  ])
//	  ->first();

//        for ($i=0;$i<$count;$i++){
//            dd($tmp[$i]['question_id']);
//        }

//        dd($request->data);

        DB::beginTransaction();

        try {
            for ($i=0;$i<$count;$i++){
                $exam = Test::whereSession($tmp[$i]['session'])->first();

                $data = HeAnswer::updateOrCreate([
                    'test_id' => $exam->id,
                    'question_id' => $tmp[$i]['question_id']
                ], [
                    'question_id' => $tmp[$i]['question_id'],
                    'test_id' => $exam->id,
                    'he_answer' => $tmp[$i]['he_answer']
                ]);

            }


            DB::commit();
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 200);
            // something went wrong
        }

        return response()->json([
          'data' => $data,
          'status' => 'success',
          'message' => 'success create answer'
        ], 200);
    }

}
