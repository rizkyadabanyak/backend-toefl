<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Question;
use App\Models\Space;
use Illuminate\Http\Request;

class SpaceController extends Controller
{
    public function destroy(Request $request){
        $request->validate([
            'id' => 'required'
        ]);


        $data = Space::find($request->id);
        $data->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'success delete data'
        ], 200);
    }
    public function create(Request $request){


        $input = $request->long_question;
//        dd($request->file('long_question'));

        if ($request->file('long_question')){

            $validates = [
                'long_question'  => 'mimes:mp3,ogg | max:20000'
            ];
            $request->validate($validates);

            $file = $request->file('long_question');

            $name = rand(999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
            $input = 'audio/'.$newName;
            $request->long_question->move(public_path('audio'), $newName);
        }

        $data = Space::updateOrCreate([
            'id' => $request->id
        ], [
            'category_id' => $request->category_id,
            'long_question' => $input,
            'section' => $request->section,

        ]);

        return response()->json([
            'data' => $data,
            'status' => 'success',
            'message' => 'success create space'
        ], 200);
    }
    public function show (Request $request){
        $data = Category::whereId($request->category_id)->with(['spaces'])->get();

        return response()->json([
	  'data' => $data,
	  'status' => 'success',
	  'message' => 'success create space'
        ], 200);
    }


}
