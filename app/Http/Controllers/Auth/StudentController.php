<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Referral;
use App\Models\Student;
use App\Models\Test;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class StudentController extends Controller
{
    public function resetPassword(Request $request){
        $request->validate([
            'oldPassword' => 'required',
            'newPassword' => 'required',
        ]);

        $data = Student::whereToken($request->token)->first();

//        dd($data);
//        dd(Hash::make('123123'));
        $oldPassword = $request->oldPassword;
        $newPassword = $request->newPassword;
//        dd(Hash::check($oldPassword, $data->password));

        if ($data && Hash::check($oldPassword, $data->password)) {

            $data->password = Hash::make($newPassword);
            $data->save();

            return response()->json([
                'message' => 'success create new password',
                'status' => 'success'
            ], 200);

        }else{
            return response()->json([
                'message' => 'old password not correct',
                'status' => 'danger'
            ], 401);
        }


    }


    public function login(Request $request)
    {
//        dd('dqw');
        $user = Student::whereEmail($request->email)->first();

        if ($user && Hash::check($request->password, $user->password)) {
            $user->token = Hash::make($request->email);
            $user->save();

              $dt = Carbon::now();
              $msg = 'anda login student pada ' . $dt->toDateTimeString();

        //	  $this->email($request->email,$msg);
              $status_email =$this->email('lingvilletest@gmail.com',$msg);

                    return response()->json([
                        'status' => 'success',
                        'token' => $user->token,
                        'message' => 'success login',
                  'status_email' => $status_email
              ], 200);
        }

        return response()->json([
            'message' => 'error'
        ], 401);
    }

    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|string|email|max:255|unique:students',
            'password' => 'required',
            'referral' => 'required',
        ]);

        $data = Referral::where('referral',$request->referral)->whereStatus('active')->count();

        if ($data == 0){
            return response()->json([
                'status'=>'danger',
                'message'=>'referral expired'
            ]);
        }else{
            $referral = Referral::where('referral',$request->referral)->first();
            $referral->status = 'non-active';
            $referral->save();
        }

        $data = new Student();

        $data->img = 'default';
        $data->token = Hash::make($request->email);
        $data->name = $request->name;
        $data->email = $request->email;
        $data->password = Hash::make($request->password);
        $data->status = 'active';

        $data->save();


        $dt = Carbon::now();
        $msg = 'anda berhasil register student pada ' . $dt->toDateTimeString();

        $status_email =$this->email($request->email,$msg);

        return response()->json([
            'data' => $data,
            'status' => 'success',
            'message' => 'success regis for student',
            'status_email' => $status_email
        ], 200);
    }
    public function profile(Request $request){
        $data = Student::select(['id','name','email','img'])->whereToken($request->token)->first();
        $test = Test::where('student_id',$data->id)->orderBy('score','DESC')->first();
        $sa =  Test::where('student_id',$data->id)->get();

        $toefl = 0;
        $pre_test =0;


        foreach ($sa as $a){
            if ($a->categories->categories == 'toefl'){
                $toefl = $toefl + 1;
            }else{
                $pre_test = $pre_test + 1;
            }
        }
        if ($test != null){
	  $score = $test->score;
        }else{
	  $score = null;
        }

        return response()->json([
            'data' => $data,
            'maxScore' => $score,
            'totalToefl' => $toefl,
            'totalPre_test' => $pre_test,
            'status' => 'success',
            'message' => 'success get data'
        ], 200);
    }

    public function EditProfileImg(Request $request){

//        return response()->json([
//	  $request->file('img')
//        ]);

        $request->validate([
            'img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

//        return response()->json([
//	  $request->file('img')
//        ]);

        $data = Student::select(['id','name','email','img'])->whereToken($request->token)->first();

        if ($request->file('img')){


            $file = $request->file('img');

            $name = rand(999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
            $input = 'profile/'.$newName;
            $request->img->move(public_path('profile'), $newName);
        }else{
            $input = $request->img;
        }

        $data->img =$input;

        $data->save();

        return response()->json([
            'data' => $data,
            'status' => 'success',
            'message' => 'success edit img profile'
        ], 200);

    }
    public function EditProfile(Request $request){
        $request->validate([
//            'img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name' => 'required'
        ]);
        $data = Student::select(['id','name','email','img'])->whereToken($request->token)->first();

//        if ($request->file('img')){
//
//
//            $file = $request->file('img');
//
//            $name = rand(999999,1);
//            $extension = $file->getClientOriginalExtension();
//            $newName = $name.'.'.$extension;
//            $input = 'profile/'.$newName;
//            $request->img->move(public_path('profile'), $newName);
//        }else{
//            $input = $request->img;
//        }

        $data->name = $request->name;
//        $data->img =$input;

        $data->save();

        return response()->json([
            'data' => $data,
            'status' => 'success',
            'message' => 'success edit profile'
        ], 200);

    }

    public function logout(Request $request){

        $data = Student::whereToken($request->token)->first();
        $data->token = null;
        $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'success logout'
        ], 200);

    }
}
