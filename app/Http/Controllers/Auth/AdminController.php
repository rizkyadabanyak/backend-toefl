<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Student;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{

    public function login(Request $request)
    {
//        dd('dqw');ddqw
        $user = User::whereEmail($request->email)->first();


//        dd('dqwdwq');
        if ($user && Hash::check($request->password, $user->password)) {
            $user->token = Hash::make($request->email);
            $user->save();

//            return response()->json($user);
            $dt = Carbon::now();
            $msg = 'anda login pada ' . $dt->toDateTimeString();

            $this->email($request->email,$msg);
	  $status_email =$this->email($request->email,$msg);




	  return response()->json([
                'status' => 'success',
	      'token' => $user->token,
                'message' => 'success login',
	      'status_email' => $status_email

	  ], 200);
        }

        return response()->json([
            'status' => 'danger',
            'message' => 'user not found'
        ], 401);
    }

    public function register(Request $request)
    {
        $data = new User();

        $request->validate([
            'name' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required',
        ]);
        $data->token = Hash::make($request->email);
        $data->name = $request->name;
        $data->email = $request->email;
        $data->password = Hash::make($request->password);
        $data->save();

        $dt = Carbon::now();
        $msg = 'anda berhasil register pada ' . $dt->toDateTimeString();

        $status_email =$this->email($request->email,$msg);

        return response()->json([
            'data' => $data,
            'status' => 'success',
            'message' => 'success create user',
	  'status_email' => $status_email
        ], 200);
    }
    public function profile(Request $request){
        $data = User::select(['name','email'])->whereToken($request->token)->first();

        return response()->json([
            'data' => $data,
            'status' => 'success',
            'message' => 'success get data'
        ], 200);

    }
    public function logout(Request $request){

        $data = User::whereToken($request->token)->first();
        $data->token = null;
        $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'success logout'
        ], 200);

    }

}
