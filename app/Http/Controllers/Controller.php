<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function email ($email,$msg){

//        header("Access-Control-Allow-Origin: *");
//        header("Content-Type: application/json; charset=UTF-8");
//        header("Access-Control-Allow-Methods: POST");
//        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        $input = json_decode(file_get_contents("php://input"), true);

        // echo "kkk";
        //$id_surat = $input["dataIdSurat"];
        //echo json_encode($input);
        //return;
        //echo json_encode('Pesan email sudah terkirim.');

        ini_set( 'display_errors', 1 );
        error_reporting( E_ALL );
        $from = "admin@lingvilletest.com";
        $to = $email;
        $subject = "Terdapat aktifitas pada webiste lingvile";
        //    $message = "Kepada Yth. " . $input['nama'] ." " ." anda mendapatkan surat masuk baru dengan perihal : " . $input['perihal'];
        $message = $msg;
        $headers = "From:" . $from;
        mail($to,$subject,$message, $headers);

//        dd('dqw');
        return 'Pesan email sudah terkirim.';

    }
}
