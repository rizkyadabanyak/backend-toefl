<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        if ($request->token == null){
            return response()->json([
                'status'=>'danger',
                'message'=>'token not found'
            ]);
        }
        $data = User::where('token',$request->token)->count();

        if ($data == 0){
            return response()->json([
                'status'=>'danger',
                'message'=>'token expired'
            ]);
        }
        return $next($request);
    }
}
